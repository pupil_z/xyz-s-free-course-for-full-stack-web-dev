package fun.chutianshu;

public class Person {
    //字段 ,普通的字段，归属于独立的对象自身，互相不能共享
    public String name;
    public int age;
    //静态字段 number，用来记录一共有多少个 Person 类对象被创建
    //静态字段或方法，不属于单个对象，而是归属于整个类所共用
    //通常使用时，可以用类名直接来访问
    public static int number=0;
    // 方法
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        Person.number++;
    }

    public void print(){
        System.out.println("姓名"+this.name+",年龄"+this.age+",当前一共有"+Person.number+"个人类对象");
    }

    //给Person类增加字段和方法
    //先增加两个字段 基础攻击 随机攻击
    // 每次打出的攻击是 ： 基础攻击 bap + 随机攻击上限 rap
    // 写一个 attack 方法，输出每次实际攻击时的攻击力
    private int bap;
    private int rap;
    public Person(String name, int age,int bap,int rap) {
        this.name = name;
        this.age = age;
        Person.number++;
        this.bap = bap;
        this. rap = rap;
    }

    public int getBap() {
        return bap;
    }

    public void setBap(int bap) {
        this.bap = bap;
    }

    public int getRap() {
        return rap;
    }

    public void setRap(int rap) {
        this.rap = rap;
    }

    public void attack(){
        //sumAp是攻击力的和，是实际的攻击力，由基础攻击+随机攻击
        //随机攻击：0-rap
        int sumAp= this.bap+(int)(Math.random()*(rap+1));
        System.out.println(this.name+"本次攻击力为："+sumAp);
    }
}
