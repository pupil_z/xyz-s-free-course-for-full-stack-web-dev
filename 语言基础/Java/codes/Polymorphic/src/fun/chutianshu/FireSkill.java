package fun.chutianshu;

//定义一个抽象类，火焰怪物，可以释放火焰魔法
// 因为 java 中，不允许同时继承子多个类（每个类只能有一个父类），如果一个类，还想继承其他非父类中的功能，就需要从“接口”获取

// 可以将一些抽象方法 封装在一个接口中，其他类，通过继承接口，来继承这些功能
public interface FireSkill {
    void Fire();
}
