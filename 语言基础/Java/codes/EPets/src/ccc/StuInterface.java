package ccc;

public interface StuInterface {
    /**
     * 查询所有学生
     */
    void selectAll();

    /**
     * 根据学生姓名，查询学生
     * @param name
     */
    void selectByName(String name);

    Student selectById(int  id);

    /**
     * 添加新学生
     * @param stu
     */
    boolean addStudent(Student stu);

    /**
     * 修改学生信息
     * @param stu
     */
    boolean editStudent(Student stu);

    /**
     * 根据学生编号，删除指定学生
     * @param id
     */
    boolean delStudentById(int id);

}
