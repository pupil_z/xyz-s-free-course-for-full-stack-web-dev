package com;

public class Car extends Vehicle{
    private String brandType;
    public Car(String type, String brand, String no, double money, String brandType) {
        super(type, brand, no, money);
        this.brandType = brandType;
    }

    public String getBrandType() {
        return brandType;
    }

    public void setBrandType(String brandType) {
        this.brandType = brandType;
    }
    @Override
    public double calRent(int days) {
        //折扣
        double discount=1;
        if(days<=7){
            discount=1;
        }else if(days>7&&days<=30){
            discount=0.9;
        }else if(days>30&&days<=150){
            discount=0.8;
        }else{
            discount=0.7;
        }
        return this.getMoney()*days*discount;
    }
}
