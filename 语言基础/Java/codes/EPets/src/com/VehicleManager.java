package com;

import java.util.Scanner;

public class VehicleManager {
    private Vehicle[] vehicles=new Vehicle[8];
    public VehicleManager() {
        //多态，父类指向子类对象
        vehicles[0]=new Car("轿车","宝马","京NY28588",800,"X6");
        vehicles[1]=new Car("轿车","宝马","京CNY3284",600,"550i");
        vehicles[2]=new Car("轿车","别克","京NT37465",300,"林荫大道");
        vehicles[3]=new Car("轿车","别克","京NT96969",600,"GL8");

        vehicles[4]=new Bus("客车","金杯","京6566754",800,16);
        vehicles[5]=new Bus("客车","金龙","京8696997",800,16);
        vehicles[6]=new Bus("客车","金杯","京9696996",1500,34);
        vehicles[7]=new Bus("客车","金龙","京8696998",1500,34);
    }
    public void operator(){
        Scanner input = new Scanner(System.in);
        System.out.println("============欢迎使用车辆租赁系统=================");
        System.out.println("1、轿车      2、客车");
        System.out.print("请选择要租赁汽车的类型:");
        int type=input.nextInt();
        if(type==1){
            System.out.print("请选择轿车品牌：1、宝马   2、别克");
            int n=input.nextInt();
//            String brand=n==1?"宝马":"别克";
            String brand="";
            String cx="";
            if(n==1) {
                brand = "宝马";
                System.out.print("请选择车型:1、X6    2、550i");
                cx=input.nextInt()==1?"X6":"550i";
            }else{
                brand = "别克";
                System.out.print("请选择车型:1、林荫大道    2、GL8");
                cx=input.nextInt()==1?"林荫大道":"GL8";
            }
            System.out.print("请输入要租赁的天数：");
            int days=input.nextInt();
            //通过上面输入的品牌、车型去获得对应的对象，获得日租金、调用计算租金的方法
            for (int i = 0; i < vehicles.length; i++) {
                //循环的时候判断vehicle的真实类型
                if(vehicles[i] instanceof Car){
                    //向下转型
                    Car car=(Car) vehicles[i];
                    if(car.getBrand().equals(brand)&&car.getBrandType().equals(cx)){
                        //做租赁的业务
                        double result=car.calRent(days);
                        System.out.println("分配您的汽车的车牌号是："+car.getNo());
                        System.out.println("您要付的日租金是："+result);
                        break;
                    }
                }
            }

        }else if(type==2){
            System.out.print("请选择客车品牌：1、金龙   2、金杯");
            String brand=input.nextInt()==1?"金龙":"金杯";

            System.out.print("请选择客车座位数:1、16座    2、34座");
            int seat=input.nextInt()==1?16:34;

            System.out.print("请输入要租赁的天数：");
            int days=input.nextInt();
            //通过上面输入的品牌、座位数去获得对应的对象，获得日租金、调用计算租金的方法
            for (int i = 0; i < vehicles.length; i++) {
                //循环的时候判断vehicle的真实类型
                if(vehicles[i] instanceof Bus){
                    //向下转型
                   Bus bus=(Bus) vehicles[i];
                    if(bus.getBrand().equals(brand)&&bus.getSeat()==seat){
                        //做租赁的业务
                        double result=bus.calRent(days);
                        System.out.println("分配您的汽车的车牌号是："+bus.getNo());
                        System.out.println("您要付的日租金是："+result);
                        break;
                    }
                }
            }

        }
    }
}
