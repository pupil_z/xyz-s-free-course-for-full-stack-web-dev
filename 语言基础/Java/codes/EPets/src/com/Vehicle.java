package com;

public abstract class Vehicle {
    //属性
    //车辆的类型
    private String type;
    //品牌
    private String brand;
    //车牌号
    private String no;
    //日租金
    private  double money;

    public Vehicle(String type, String brand, String no, double money) {
        this.type = type;
        this.brand = brand;
        this.no = no;
        this.money = money;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
    //父类中的抽象方法
    public abstract double calRent(int days);
}
