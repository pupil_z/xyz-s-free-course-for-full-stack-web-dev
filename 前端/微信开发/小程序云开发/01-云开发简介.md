# 微信小程序云开发简介

## 1. 云开发特点

和普通微信小程序相比，不需要写服务器端，可以直接使用微信提供的后端（服务器端） Web 服务，而无需自行编写 web services，自然也不需要搭建服务器，可免鉴权直接使用平台提供的 API 进行业务开发。

![](../../../imgs/miniprogram_01.png)

* 优点：
  - 自然是不需要服务器端
  - 免登录、免鉴权调用微信开放服务
* 缺点：
  - 可以使用的微信平台提供的 API 自然是有限制的，不可能做到随心所以，相比自行开发来说，功能受限
  - 微信平台 API 需要付费

## 2. 快速入门

### 2.1 注册并登录微信开放平台账号（非必要）

微信开放平台地址：https://open.weixin.qq.com/ ，是微信的 “ 开发者 ” 平台



> 注意：
> 要注意区分公众平台（写公众号文章、配置公众号）和开放平台（开发者使用）

可以绑定并管理小程序

### 2.2 注册小程序

每个小程序都必须有一个 APPID （所有小程序都必须具备的唯一标识），所以使用前也许要注册。

除非使用测试号，因为小程序注册有数量限制，所以在开发阶段可以使用测试号，发布时，就需要有效的 AppID了。

[小程序注册上限和绑定上限](https://kf.qq.com/faq/170109F3MRFj170109eYJ7fi.html)

另外需要注意：
* 一个邮箱只能绑定一个小程序。
* 相同主体：上限5个，绑定次数不限。
* 未认证帐号不支持绑定不同主体的小程序。
* 个人账号不支持认证，所以如果是个人账号，需要使用主体相同的微信账号和公众平台账号

方式 1 ：可以直接使用小程序开发工具内部链接注册小程序
![](../../../imgs/minipro_reg.png)

方式2 ： 可以通过公众平台链接，注册小程序  
地址 ：[小程序注册链接](https://mp.weixin.qq.com/wxopen/waregister?action=step1)

![](../../../imgs/wx_mini_reg.png)

### 2.3 测试号注册

和上面小程序注册相同，核心相同，只不过是不同入口。

也是两种：入口1，直接在小程序开发工具内部点击链接；入口2，小程序的注册页面，对应链接：https://mp.weixin.qq.com/wxamp/sandbox

扫码：
![](../../../imgs/wx_mini_testid_reg.png)

注册后：
![](../../../imgs/wx_mini_testid_reg1.png)

一个微信号，应该也只能注册一个测试号

使用测试号创建的小程序项目：

![](../../../imgs/wx_mini_testid_reg2.png)