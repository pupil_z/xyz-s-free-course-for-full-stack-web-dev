# 导航

## 1. 导航 nav

### 1.1 构建 bootstrap 导航的步骤

1. 使用 nav 或 ul ，挂接 .nav 类
2. 每个导航项使用挂接了 .nav-item 的li，
3. li 中再放上挂接了 .nav-link 的 a 
4. 如果需要二级菜单，将另一个 ul 或 nav 嵌套包含在一个 li 中，并挂接样式 .dropdown-menu；导航项 a 标签，挂接样式 .dropdown-item；二级菜单默认为下拉纵向外观
5. 一般导航实现，只需要挂接 bootstrap 的 css 库即可实现，但如果要使用功能型导航，如tab（分页）或 pill（枕头）导航，就需要同时挂接 bootstrap 的 js 库，实现在不同分页间跳转时，内容的切换效果

> nav 和 ul 区别：  
> nav 是 html5 中新增 标签，语义标签，功能上几乎没有区别，更方便代码阅读、seo

### 1.2 相关 bootstrap 样式

1. .active ：当前所在导航，挂接在 a 上；
2. ul或nav 用 flex 类，来设置导航对齐方式，如：
   *  .justify-content-center：居中
   *  .justify-content-end：右对齐
   *  .justify-content-start：左对齐
3. ul 或 nav 挂接 .flex-column，切换为纵向，变体有：.flex-sm-column、.flex-lg-column
4. ul 或 nav 挂接 .nav-tabs，切换为分页外观模式
5. ul 或 nav 挂接 .nav-pills，挂接为“枕头”外观模式，辅助外观样式有：
   * .nav-fill : 导航项背景色完全填充；
   * .nav-justified：导航项等宽，背景色完全填充

### 1.3 实现 tab 分页 / pill 分页

1. ul 或 nav 挂接 .nav 和 .nav-tabs 类；或者 .nav-pills
2. ul 或 nav 设置 role 属性为 tablist / tab / tabpanel
3. 不能包含 dropdown 下拉菜单
4. 分页项为了响应点击切换事件，需要使用 button 替换 a ，设置如下：
``` html
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Home</button>
    <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</button>
    <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</button>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">home 分页</div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">profiel 分页</div>
  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">contact 分页</div>
</div>
```
> 说明：
> 1. data-bs-toggle 属性值需要设置为 tab 或 pill
> 2. data-bs-target 属性值需要设置为当前分页挂接的内容层 id 值
> 3. 设置好 data-bs 数据属性后，直接会挂接 bootstrap 中的 js 库，实现分页动态效果，一句自定义的 javascript 脚本都不需要在写

### 1.4 自定义 javascript

#### 1.4.1 不用 data-bs-toggle ，操作 tab 或 pill 

不适用 data-bs-toggle ，自己写 js


``` javascript
// 显示 tab 内容的方式
var triggerTabList = [].slice.call(document.querySelectorAll('#myTab button'))
triggerTabList.forEach(function (triggerEl) {
  var tabTrigger = new bootstrap.Tab(triggerEl)

  triggerEl.addEventListener('click', function (event) {
    event.preventDefault()
    tabTrigger.show()
  })
})

// 选取子项并显示的方式
var triggerEl = document.querySelector('#myTab button[data-bs-target="#profile"]')
bootstrap.Tab.getInstance(triggerEl).show() // Select tab by name

var triggerFirstTabEl = document.querySelector('#myTab li:first-child button')
bootstrap.Tab.getInstance(triggerFirstTabEl).show() // Select first tab
```

#### 1.4.2 tab 或 pill 常用方法

1. 构造函数：new bootstrap.Tab()
2. 显示函数：tab对象.show()
3. 销毁：tab.dispose()
4. 静态方法，获取或创建：bootstrap.Tab.getOrCreateInstance()

#### 1.4.3 事件

当一个新分页显示时, 一次激活以下四个事件:

1. hide.bs.tab (on the current active tab)
2. show.bs.tab (on the to-be-shown tab)
3. hidden.bs.tab (on the previous active tab, the same one as for the hide.bs.tab event)
4. shown.bs.tab (on the newly-active just-shown tab, the same one as for the show.bs.tab event)

``` js
var tabEl = document.querySelector('button[data-bs-toggle="tab"]')
tabEl.addEventListener('shown.bs.tab', function (event) {
  event.target // newly activated tab
  event.relatedTarget // previous active tab
})
```

## 2. 导航条 navbar

导航条 navbar 是更加复杂的导航控件，通过 .navbar 挂接到 nav 标签上，这里的 nav 只是一个容器，类似于层 div ，内部怎么设计都可以

### 2.1 导航栏使用步骤

1. nav 标签挂接样式 .navbar , 配合 .navbar-expand{-sm|-md|-lg|-xl|-xxl}的响应式布局和配色方案类；
2. nav 内部一般配合一个样式为 .container-* 的 div
3. div 内部，真正的导航部分(比如 ul)需要挂接样式 .navbar-nav
4. 每个导航项（比如 li），需要挂接样式 .nav-item 
5. 具体导航链接 a ，需要挂接样式 .nav-link
6. navbar 中，可以包含 form 表单（比如设置搜索栏）

### 2.2 折叠

