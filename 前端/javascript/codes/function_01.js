//定义函数，参数是一个对像
function changeCar(newCar) {
    //将对象中的一个属性make的重新赋值
    newCar.make = "比亚迪";
    newCar.model = "汉Pro"
    newCar.year = 2025;
}

//定义一个对象，存储了我的车辆信息
var myCar = {make: "三菱", model: "蓝瑟", year: 2012};

//定义两个变量
var x, y;

// 将旧车品牌赋值给x
x = myCar.make;     // x 获取的值为 "Honda"

// 换新车
changeCar(myCar);
// 将旧车品牌赋值给y
y = myCar.make;     // y 获取的值为 "Toyota"

//如果参数类型不对如何？
var a = 5;
changeCar(a)
var z = a.make

// 显示信息
function showInfo(){
    alert("旧车信息："+x)
    alert("新车信息:"+y)
    alert(a);
}
