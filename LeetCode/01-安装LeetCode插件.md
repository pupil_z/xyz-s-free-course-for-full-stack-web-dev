# 安装 LeetCode 插件

LeetCode 在网站上刷题不是很方便，一般通过编译器就可以快速配置刷题环境，下文中，展示在两种常用IDE中安装使用 LeetCode 插件的方式

## 0. 注册 LeetCode

首先，到 https://leetcode.cn/ 注册自己的账号，如果外网网速可以，并且有需要的话（比如在外留学的小伙伴），可以选择 https://leetcode.com/ 这个是国外主站。

国内站：

![](../imgs/leetCodeCn.png)

国外站：
![](../imgs/leetCodeCom.png)

## 1. VS Code

### 1.1 安装及简介

Ctrl + Shift + X 打开扩展窗口，直接搜索 Leetcode，有很多可选插件，从上到下，一般第一个就是官方的。

![](../imgs/vscode-leetcode-ext.png)

通常有两类基于官方插件的扩展插件可供选择：

1. 第一个单词是 Debug 的，增强的，可以用来进行 Debug 的 LeetCode 插件。LeetCode 插件本身提供的算法片段是不能直接运行的。但注意，这些 Debug 插件有一些语言限制，大部分只支持少量主流代码，如 c、C++、Python、java 等

2. 还有一些扩展插件，基本上属于信息类增强，界面更加优化的，比如 LeetCode with labuladong ，不过可能附加些知识消费的私货，也可以理解

大家可以根据自身需求，进行选择。我这里主要介绍官方插件（Vs code）和labuladong插件（Idea）的用法。

### 1.2 基本使用

安装完插件后，左侧侧边栏会出现 LeetCode 图标

![](../imgs/vscode-leetcode-ext-1.png)

然后，选择服务器
![](../imgs/vscode-leetcode-ext-2.png)
并登录
![](../imgs/vscode-leetcode-ext-3.png)
其他几个登录选项是使用Github、领英、以及Cookie登录

打开左侧题目列表，开始开心地刷吧 :blush:
![](../imgs/vscode-leetcode-ext-4.png)

## 2. IDEA

IDEA中，我安装的是基于官方 LeetCode 的插件的扩展插件

![](../imgs/idea-leetcode-ext-1.png)

国内一个知识自媒体大佬做的，要比官方的更加易用些，虽然里面包含了部分知识付费内容，但是插件本身质量还是不错的。

装上后，打开插件窗口，本身就自带使用说明

![](../imgs/idea-leetcode-ext-2.png)

点开插件中的设置按钮，设置账号，就可以直接登录开刷

![](../imgs/idea-leetcode-ext-3.png)

## 3. 刷题个人建议

个人建议，刷题如果想要效果更好的话，最好还是在 IDE 中为每套题目自建真实项目（用插件可以不建），把觉得不错的算法都添加进去，进行比对。

比如我的这个系列中，主要使用3种语言：Java 、 Rust 、 Typescript

java 会用IDE创建项目，目录如下：

![](../imgs/idea-leetcode-ext-4.png)

优点：
* 不需记笔记，算法及优劣一目了然
* 方便调试 Debug
* 方便管理