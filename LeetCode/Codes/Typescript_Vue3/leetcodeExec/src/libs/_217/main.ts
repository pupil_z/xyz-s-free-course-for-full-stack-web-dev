import { Solution } from './solution_1'

export function main(): void {
    var nums: number[] = [1, 2, 2, 5, 8]
    let solution: Solution = new Solution()
    console.log(solution.containsDuplicate(nums))
}
