public class Solution_1 {
    public ListNode removeElements(ListNode head, int val) {
        ListNode now = head;
        ListNode pre = new ListNode(-1);

        pre.next = now;


        while (now!=null)
        {
            if(now.val==val)
            {
                pre.next = now.next;
                if(pre.val==-1) {

                    head = now.next;
                }
            }
            else {
                pre = now;
            }
            now = now.next;
        }
        return head;
    }
}
