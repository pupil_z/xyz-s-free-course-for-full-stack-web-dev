import java.util.ArrayList;
import java.util.List;
/*
* 线性递推
*
*
*
* */
public class Solution {


    public List<Integer> getRow(int rowIndex) {
        List<Integer> list = new ArrayList<>();

        list.add(1);
        for (int i=1;i<=rowIndex;i++) {
            list.add((int)((long)list.get(i-1)*(rowIndex-i+1)/i));
        }

        return list;
    }
}
