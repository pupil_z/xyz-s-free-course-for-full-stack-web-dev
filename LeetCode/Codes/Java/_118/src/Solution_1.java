import java.util.ArrayList;
import java.util.List;

//使用 ArrayList add(num) 方法
public class Solution_1 {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();

        for(int i = 1;i<=numRows;i++)
        {
            List<Integer> rowList = new ArrayList<>();
            if(i==1)
            {
                rowList.add(1);
            }
            if(i==2) {
                rowList.add(1);
                rowList.add(1);
            }
            else
            {
                rowList.add(1);
                for(int j=1;j<i-1;j++)
                {
                    rowList.add(result.get(i-2).get(j-1)+result.get(i-2).get(j));
                }
                rowList.add(1);
            }
            result.add(rowList);
        }
        return result;
    }
}
