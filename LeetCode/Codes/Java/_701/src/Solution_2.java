/*
* 算法：递归
*
* 二叉搜索树的性质：对于任意节点 root 而言，左子树（如果存在）上所有节点的值均小于 root.val，
* 右子树（如果存在）上所有节点的值均大于 root.val
* 思路：
* 1. 先判断值的大小，然后按照左右枝进行递归
* 2. 重点：先判断大小，在进行递归


* */
public class Solution_2 {
    public TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        if(val< root.val)
        {
            root.left=insertIntoBST(root.left,val);
        }
        else {
            root.right=insertIntoBST(root.right,val);
        }

        return root;
    }
}
