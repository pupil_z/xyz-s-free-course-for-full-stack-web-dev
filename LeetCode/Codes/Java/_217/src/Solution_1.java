public class Solution_1 {
    public boolean containsDuplicate(int[] nums){
        int i=0;
        while (i<nums.length){
            int x=nums[i];
            i++;
            for(int j=i;j<nums.length;j++) {
                if (x == nums[j]) {
                    return true;
                }
            }
        }
        return false;
    }


}
