//优化：因为两个数组如果互为异位此，则长度必须相等，所以可以节省一个循环体
public class Solution_2 {
    public boolean isAnagram(String s, String t) {
        if(s.length()!=t.length())
        {
            return false;
        }
        int[] m = new int[26];
        for (int i = 0; i < s.length(); i++) {
            m[s.charAt(i) - 'a']++;
            m[t.charAt(i) - 'a']--;
        }

        for (int i = 0; i < 26; i++) {
            if (m[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
