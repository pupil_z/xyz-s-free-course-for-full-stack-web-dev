//暴力思路
//不考虑性能，最直接遍历
// 1. 两层循环，取到所有的项
// 2. 遍历周围8个位置，新建一个同大小矩阵，存储新项
public class Solution_1 {
    public void gameOfLife(int[][] board) {
        int m = board.length;
        int n = board[0].length;

        int [][] newBoard = new int[m][n];

        for (int i = 0; i < m ; i++) {
            for (int j = 0; j < n; j++) {
                int count=0;
                if(i-1>=0&&j-1>=0)
                {
                    count+=board[i-1][j-1];
                }
                if(i-1>=0)
                {
                    count+=board[i-1][j];
                }
                if(i-1>=0&&j+1<n)
                {
                    count+=board[i-1][j+1];
                }
                if(j-1>=0)
                {
                    count+=board[i][j-1];
                }
                if(j+1<n)
                {
                    count+=board[i][j+1];
                }
                if(i+1<m&&j-1>=0)
                {
                    count+=board[i+1][j-1];
                }
                if(i+1<m)
                {
                    count+=board[i+1][j];
                }
                if(i+1<m&&j+1<n)
                {
                    count+=board[i+1][j+1];
                }
                if(board[i][j]==1)
                {
                    if(count==2||count==3)
                    {
                        newBoard[i][j]=1;
                    }
                    else {
                        newBoard[i][j]=0;
                    }
                }
                else {
                    if (count == 3) {
                        newBoard[i][j] = 1;
                    } else {
                        newBoard[i][j] = 0;
                    }
                }
            }
        }

        for(int i=0;i<m;i++)
        {
            for(int j=0;j<n;j++)
            {
                board[i][j]=newBoard[i][j];
            }
        }
    }


}
