// 思路：迭代法
// x 指向源链表
// m n 指向新链表前两位
// 迭代移动 x m n ，遍历源链表，生成新链表
public class Solution_1 {
    public ListNode reverseList(ListNode head) {
        ListNode x = head;
        ListNode m = null;
        ListNode n = null;

        while(x!=null)
        {
            n=m;
            m = x;
            x = x.next;
            m.next = n;
        }

        return m;
    }
}
