/*
* 给定一个包含红色、白色和蓝色、共 n 个元素的数组 nums ，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。

我们使用整数 0、 1 和 2 分别表示红色、白色和蓝色。

必须在不使用库内置的 sort 函数的情况下解决这个问题。



示例 1：

输入：nums = [2,0,2,1,1,0]
输出：[0,0,1,1,2,2]
示例 2：

输入：nums = [2,0,1]
输出：[0,1,2]


提示：

n == nums.length
1 <= n <= 300
nums[i] 为 0、1 或 2


进阶：

你能想出一个仅使用常数空间的一趟扫描算法吗？
*
*  三个指针，第一个记录最后一个0的位置+1，第二个是当前正在遍历的位置，第三个是第一个2的位置-1
*  遍历指针如果遇到0，遍历指针位置填入（最后一个0位置+1 的值），最后一个0位置 指针向后移；
*  遍历指针如果遇到1，则不管，继续向后；
*  遍历指针如果遇到2，遍历指针位置填入（第一个2位置-1 的值），第一个2位置前移
*
*
* */


public class Solution_1 {
    public void sortColors(int[] nums) {
        //数组长度
        int length = nums.length;
        //最后一个0指针索引位置初始值设为 -1
        int last0=-1;
        //第一个2指针索引位置初始值设为 length
        int first2=length;
        //当前索引位设置为 i
        int i=0;

        //设置循环结束条件
        while(i<first2)
        {
            if(nums[i]==0)
            {
                if(i<=last0)
                {
                    i=last0+1;
                    continue;
                }
                if(last0==length-1)
                {
                    break;
                }
                int x = nums[++last0];
                nums[last0]=0;
                nums[i]=x;
            }
            else if(nums[i]==1)
            {
                i++;
            }
            else {
                int x = nums[--first2];
                nums[first2]=2;
                nums[i]=x;
            }
        }

        for (int x: nums) {
            System.out.println(x);
        }

    }
}
