import java.util.Iterator;
import java.util.LinkedList;

public class MyHashSet {
    private static int BASE = 769;
    private LinkedList<Integer> [] data;
    private static int hash(int key)
    {
        return key%BASE;
    }

    public MyHashSet(){
        LinkedList<Integer> list = new LinkedList<>();
        this.data = new LinkedList[BASE];
        for (int i=0;i<BASE;i++) {
            data[i]=new LinkedList<>();
        }
    }

    public void add(int key){
        int h = hash(key);
        Iterator<Integer> iterator = data[h].iterator();
        while (iterator.hasNext())
        {
            Integer x = iterator.next();
            if(x==key)
            {
                return;
            }
        }
        data[h].add(key);
    }

    //在操作链表的同时，不能使用遍历来进行
    //因为如果增减链表项，会破坏遍历的索引，导致遍历失败
    //foreach遍历只能用在读取上，而不能用在删除和插入上
    //如果对链表进行删除和插入操作，需要使用到迭代器 Iterator
    public void remove(int key){
        int h = hash(key);
        Iterator<Integer> iterator = data[h].iterator();
        while (iterator.hasNext())
        {
            Integer x = iterator.next();
            if(x==key)
            {
                //不要使用 remove(key)
                //会出现错误
                data[h].remove(x);
                return;
            }
        }

    }

    public boolean contains(int key){
        int h = hash(key);
        Iterator<Integer> iterator = data[h].iterator();
        while (iterator.hasNext())
        {
            Integer x = iterator.next();
            if(x==key)
            {
                return true;
            }
        }
        return false;
    }
}
