import java.util.HashSet;
import java.util.Set;

/*
* 算法：递归
*
* 思路：
* 1. 左路是否相等，右路是否相等

* */
public class Solution_1 {
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if(root==null)
        {
            return false;
        }

        targetSum -= root.val;
        //到叶子结点时，targetSum正好被减到0，则成立
        if(targetSum == 0 && root.left == null && root.right == null) return true;
        if(hasPathSum(root.left,targetSum)||hasPathSum(root.right,targetSum))
        {
            return true;
        }
        return false;
    }
}
