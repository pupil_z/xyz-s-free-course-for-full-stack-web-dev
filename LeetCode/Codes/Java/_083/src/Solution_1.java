public class Solution_1 {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode pre = new ListNode(-101);
        ListNode curr = head;
        pre.next = curr;

        while(curr!=null)
        {
            if(pre.val==curr.val)
            {
                pre.next =curr.next;
                curr=curr.next;
            }
            else {
                pre = curr;
                curr = curr.next;
            }
        }
        return head;
    }
}
