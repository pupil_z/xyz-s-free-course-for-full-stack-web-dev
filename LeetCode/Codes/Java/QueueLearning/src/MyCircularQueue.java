import java.util.ArrayList;
import java.util.List;

/* 自定义循环队列 FIFO
 特点:
    1. data 定长
    2. 有头指针和尾指针
 */
public class MyCircularQueue<T> {
    // 队列中存储数据用到的列表
    private List<T> data;
    // 头部索引
    private int p_start;
    //尾部指针索引
    private int p_end;
    //长度
    private int length;
    public MyCircularQueue(int k){
        length = k;
        data = new ArrayList<T>(length);
        p_start = 0;
        p_end=-1;
    }
    // 入队
    public boolean enQueue(T x){
        if(this.IsFull())
        {
            return false;
        }
        if(p_end!=length-1) {
            p_end++;
        }
        else {
            p_end=0;
        }
        data.add(x);
        return true;
    }
    //出队
    public boolean deQueue(){
        if(this.IsEmpty()){
            return false;
        }
        data.remove(p_start);
        if(p_start!=length-1)
        {
            p_start++;
        }
        else {
            p_start=0;
        }
        return false;
    }

    //获取头元素值
    public T GetFront(){
        return data.get(p_start);
    }

    //获取尾部元素值
    public T GetRear(){
        return data.get(p_end);
    }
    //队列是否为空
    public boolean IsEmpty(){
        if(data.get(p_start)==null)
        {
            return true;
        }
        return false;
    }

    //队列是否已满
    public boolean IsFull(){
        if(p_start+p_end==length)
        {
            return true;
        }
        else {
            return false;
        }
    }
}
