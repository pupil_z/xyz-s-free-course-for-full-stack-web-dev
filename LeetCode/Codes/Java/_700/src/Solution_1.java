/*
* 算法：递归
*
* 思路：
* 1. 对当前节点左、右子节点进行递归调用
* 2. 判断当前节点是值是否和参数值先等

* */
public class Solution_1 {
    public TreeNode searchBST(TreeNode root, int val) {
        if(root==null)
        {
            return null;
        }
        if(root.val==val)
        {
            return root;
        }

        if(root.left!=null&&root.left.val==val)
        {
            return root.left;
        }
        if(root.right!=null&&root.right.val==val)
        {
            return root.right;
        }
        TreeNode l = searchBST(root.left,val);
        if (l!=null)
        {
            return l;
        }
        TreeNode r = searchBST(root.right,val);
        if (r!=null)
        {
            return r;
        }

        return null;
    }
}
