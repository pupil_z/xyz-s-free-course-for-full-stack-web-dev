public class Solution_1 {
    public boolean hasCycle(ListNode head,int pos) {
        if(head.next==null||head==null)
        {
            return false;
        }
        ListNode slow = head;
        ListNode fast=head.next;

        while(slow.next!=null)
        {
            if(fast==null||fast.next==null)
            {
                return false;
            }
            if(fast==slow)
            {
                return true;
            }
            slow=slow.next;
            fast=fast.next.next;
        }
        return false;
    }
}
