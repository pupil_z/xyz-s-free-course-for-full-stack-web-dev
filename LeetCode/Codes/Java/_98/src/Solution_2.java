/*
* 算法：递归
*
* 二叉搜索树的性质：对于任意节点 root 而言，左子树（如果存在）上所有节点的值均小于 root.val，
* 右子树（如果存在）上所有节点的值均大于 root.val
* 思路：
* 1. 先判断值的大小，然后按照左右枝进行递归
* 2. 重点：先判断大小，在进行递归


* */
public class Solution_2 {
    public boolean isValidBST(TreeNode root) {
        return help(root,Long.MIN_VALUE,Long.MAX_VALUE);

    }
    boolean help(TreeNode node,long lower,long upper)
    {
        if(node == null)
        {
            return true;
        }
        if(node.val<=lower||node.val>=upper)
        {
            return false;
        }
        return help(node.left,lower,node.val)&&help(node.right,node.val,upper);
    }
}
