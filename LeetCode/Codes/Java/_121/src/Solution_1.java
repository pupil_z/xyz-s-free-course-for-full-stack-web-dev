/*
    照旧，先来个暴力双循环：
    算法思路：
    算出任意两数之间的所有差值，取最大值

    算法分析：
    时间复杂度O(n2)，超大数组会导致超出时间限制，无法通过
    空间复杂度O(1)

 */
public class Solution_1 {
    public int maxProfit(int[] prices) {
        int max=0;
        for(int i=0;i<prices.length;i++)
        {
            for(int j=i+1;j<prices.length;j++)
            {
                if(prices[j]-prices[i]>max)
                {
                    max=prices[j]-prices[i];
                }
            }
        }
        return max;
    }
}
