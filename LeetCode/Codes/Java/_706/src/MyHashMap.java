import java.util.Iterator;
import java.util.LinkedList;

public class MyHashMap
{
    private class Entry{
        private int key;
        private int value;

        public Entry(int key,int value)
        {
            this.key=key;
            this.value=value;
        }

        public int getKey() {
            return key;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    //初始大小
    private static final int BASE = 769;
    private LinkedList[] data;

    public MyHashMap() {
        data = new LinkedList[BASE];
        for(int i =0;i<BASE;i++)
        {
            data[i]=new LinkedList<Entry>();
        }
    }
    //获取哈希值
    private static int hash(int key){
        return key % BASE;
    }
    //加入新项，如果key存在，则更新对应 value
    public void put(int key,int value)
    {
        //获取哈希值
        int h = hash(key);
        //迭代器对象
        Iterator<Entry> iterator = data[h].iterator();
        //遍历链表，查找是否存在当前项
        while (iterator.hasNext())
        {
            Entry entry=iterator.next();
            if(entry.getKey()==key){
                entry.setValue(value);
                return;
            }
        }
        //如果不存在，则增加
        data[h].add(new Entry(key,value));
    }

    public int get(int key){
        int h = hash(key);
        Iterator<Entry> iterator = data[h].iterator();
        while(iterator.hasNext())
        {
            Entry entry = iterator.next();
            if(entry.getKey()==key)
            {
                return entry.getValue();
            }
        }
        return -1;
    }

    public void remove(int key){
        int h = hash(key);
        Iterator<Entry> iterator = data[h].iterator();
        while(iterator.hasNext()) {
            Entry entry = iterator.next();
            if (entry.getKey() == key) {
                data[h].remove(entry);
                return;
            }
        }
    }
}
