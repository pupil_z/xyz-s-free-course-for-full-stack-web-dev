import java.util.HashMap;

public class Solution_1 {
    public int majorityElement(int[] nums) {
        HashMap<Integer,Integer> map = new HashMap<>();
        if(nums.length==1)
        {
            return nums[0];
        }
        for (int x: nums) {
            if(map.containsKey(x))
            {
                map.replace(x,map.get(x)+1);
                if(map.get(x)>nums.length/2)
                {
                    return x;
                }
            }
            else {
                map.put(x,1);
            }
        }
        return 0;
    }
}
