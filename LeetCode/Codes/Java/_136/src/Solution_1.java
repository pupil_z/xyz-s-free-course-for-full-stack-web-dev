import java.util.*;


public class Solution_1 {
    public int singleNumber(int[] nums) {
        Set<Integer> set = new HashSet<>();

        for (int x: nums) {
            if(set.contains(x))
            {
                set.remove(x);
            }
            else {
                set.add(x);
            }
        }
        Iterator it = set.iterator();
        while(it.hasNext())
        {
            return (int) it.next();
        }
        return 0;
    }
}
