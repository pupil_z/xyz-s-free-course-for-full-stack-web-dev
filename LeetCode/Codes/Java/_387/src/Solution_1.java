import java.util.HashMap;
import java.util.Map;

// 思路：
// 1. 创建一个HashMap<char,Int[2]>,
// 2. 遍历字符串，哈希表，依次存入字符，出现次数和索引位
// 3. 遍历HashMap，输出第一个出现次数小于2的索引位
public class Solution_1 {
    public int firstUniqChar(String s) {
        HashMap<Character,int[]> hashMap = new HashMap<>();
        char [] charArray = s.toCharArray();

        int index = -1;

        for(int i =0;i<charArray.length;i++)
        {
            //如果不存在，则添加
            if(!hashMap.containsKey(charArray[i]))
            {
                hashMap.put(charArray[i], new int[]{1, i});
            }
            //如果存在，则更改个数
            else {
                int x= hashMap.get(charArray[i])[0];
                hashMap.replace(charArray[i],new int[]{x+1,i});
            }
        }


        int x=1;
        int min = 0;
        boolean has=false;
        for(Map.Entry<Character,int[]> entry:hashMap.entrySet())
        {
            if(entry.getValue()[0]==1)
            {
                if(x==1) {
                    min = entry.getValue()[1];
                    x++;
                }
                else {
                    if(entry.getValue()[1]<=min)
                    {
                        min=entry.getValue()[1];
                    }
                }
                has=true;
            }
        }
        if(has)
        {
            return min;
        }
        else {
            return index;
        }
    }
}
