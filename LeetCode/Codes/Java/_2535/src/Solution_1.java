//算法分析
//优点：速录清晰，代码量小
//缺点：要比直接算出来的，运行效率低，时间复杂度较高，空间复杂度中等
public class Solution_1 {
    public int differenceOfSum(int[] nums) {
        int sum1=0,sum2=0;
        String s="";
        for(int i = 0 ;i<nums.length;i++)
        {
            sum1+=nums[i];
            char[] charArray = Integer.toString(nums[i]).toCharArray();
            int x=0;
            for (char c: charArray) {
                x+=(int)c-48;
            }
            sum2+=x;
        }
        return Math.abs(sum1-sum2);
    }
}
