# HashMap 

## 1. HashTable
### 1.1 概念

散列表（Hash table，也叫哈希表），是根据关键码值(Key value)而直接进行访问的数据结构。

通过把关键码值映射到表中一个位置来访问记录，以加快查找的速度。

哈希算法（也叫散列），就是把任意长度值（key）通过散列算法变换成固定长度的key(地址)， 通过这个地址进行访问的数据结构.

``` java
//哈希函数
//哈希函数(hash(Object key))计算出哈希值,通过(n - 1) & hash(哈希值)计算出桶的位置(数组的索引值),
//从代码可以看出是哈希算法根据key的hashCode值的高16位和低16位去进行异或运算而计算出hash值.
static final int hash(Object key) {
    int h;
    return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
}
```

映射函数叫做散列函数，存放记录的数组叫做散列表。通过把关键码值映射到表中一个位置来访问记录，以加快查找速度。

给定表M，存在函数f(key)，对任意给定的关键字值key，代入函数后若能得到包含该关键字的记录在表中的地址，则称表M为哈希(Hash）表，函数f(key)为哈希(Hash) 函数。

![](../imgs/c6321140c31640ba875f5f12bff7545d.png)

HashCode: 通过字符串算出它的ascii 码，进行mod(取模)，算出哈希表中的下标


### 1.2 算法优点

在列表查找中，使用最广泛的二分查找算法，复杂度为O(log2n)，但其始终只能用于有序列表。

普通无序列表只能采用遍历查找，复杂度为O(n)。

而拥有较为理想的哈希函数实现的哈希表，对其任意元素的查找速度始终为常数级，即O(1)。

通过关键值计算直接获取目标位置，对于海量数据中的精确查找有非常惊人的速度提升，理论上即使有无限的数据量，一个实现良好的哈希表依旧可以保持O(1)的查找速度，

### 1.3 应用场景

Redis 为什么那么快，主要就是其核心使用了 HashTable

## 2. HashMap

### 2.1 概念

HashMap 是较为万能的数据结构 HashTable 的 Java 实现。

特点如下：

* 每一项称为一个 Entry，由键值对 Key - Value 组成
* 可以通过 Key 快速访问指定项，存取 Value
* 允许使用null值和null键
* 无序
* key 不可重复

### 2.2 数据结构

* 在JDK7 中，由数组+链表组成

* 在JDK8 中，由数组+链表+红黑树组成。

其中 链表则是主要为了解决哈希冲突而存在的。

当链表过长，则会严重影响 HashMap 的性能，jdk8做了进一步的优化，引入了红黑树，链表和红黑树在达到一定条件会进行转换, 其中 链表的时间复杂度是 O(n)，红黑树时间复杂度是 O(logn)

> 注意：
> 影响HashMap性能的重要参数：
> * 初始容量 capacity： 创建哈希表(数组)时桶的数量，默认为 16。如果太少，很容易触发扩容，如果太多，遍历哈希表会比较慢。
> * 负载因子 loadFactor ：哈希表在其容量自动增加之前可以达到多满的一种尺度，默认为 0.75
> * 阈值 threshold 。 阈值=容量*负载因子。默认12。当元素数量超过阈值时便会触发扩容

HashMap 的数据结构是基于 数组 + 链表 + 红黑树 组成,而Entry<K,V>对象是HashMap的基本存储单位,Entry<K,V>对象是一种key-value键值对映射的数据结构对象.
  HashMap 维护着一个内部静态类 Node<K,V> 节点, 它是Entry<K,V>对象的实现类.
  也就是说,HashMap集合存储着许多个 <Node<K,V> 节点,那这些 Node<K,V> 节点 跟 数组、链表、红黑树 有何联系的呢?
  HashMap默认会初始化一个长度为16的数组(table,16个桶),根据key值的hashcode值进行哈希算法计算出一个hash值,hash值与数组的长度进行位运算计算出数组的索引值(桶的位置,0-15,哈希算法与数组长度进行位运算计算出来的数组索引值总是在数组长度的范围内),并且所有的桶默认指向一个NULL;每个桶装着要么是指向一个NULL、要么是链表、要么是一颗红黑树.链表是解决了哈希冲突,红黑树解决了链表长度过长时遍历查询效率过低的问题(下面有解释什么是哈希冲突).

假如向一个桶内插入元素,会出现三种情况 :

* 第一种情况就是如果这个桶里面指向的是一个NULL(所有的桶默认指向一个NULL,即默认桶内没有元素),则直接把当前的Node<K,V>节点添加到这个桶里面;
* 第二种情况就是当前桶内装的是一条链表(即存在元素),则需要进行遍历判断是否存在同时满足 key的hash值是否相等 且 通过==、equals()的比较进行判断key是否为相同对象 的条件,如果满足则说明是同一个key对象,替换当前插入的value值,反之则插入到链尾中;
* 第三种情况就是当前桶内的装的是一棵红黑树(即存在元素),把元素插入到红黑树中,同样也会判断是否存在同时满足 key的hash值是否相等 且 通过==、equals()的比较进行判断key是否同一个对象 的条件.

ashMap转变为红黑树的条件是 : 当出现链表长度>=8 且 数组容量>=64 时 才会把链表转红黑树

![](../imgs/HashMap-4.jpg)

### 2.3 类

public class HashMap<K,V> extends AbstractMap<K,V> implements Map<K,V>

public abstract class AbstractMap<K,V> implements Map<K,V>

public interface Map<K, V>

### 2.4 手写HashMap

#### 2.4.1 早期数组+链表版本

``` java
class MyHashMap {

private class Entry{
        private int key;
        private int value;

        public Entry(int key,int value)
        {
            this.key=key;
            this.value=value;
        }

        public int getKey() {
            return key;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    //初始大小
    private static final int BASE = 769;
    private LinkedList[] data;

    public MyHashMap() {
        data = new LinkedList[BASE];
        for(int i =0;i<BASE;i++)
        {
            data[i]=new LinkedList<Entry>();
        }
    }
    //获取哈希值
    private static int hash(int key){
        return key % BASE;
    }
    //加入新项，如果key存在，则更新对应 value
    public void put(int key,int value)
    {
        //获取哈希值
        int h = hash(key);
        //迭代器对象
        Iterator<Entry> iterator = data[h].iterator();
        //遍历链表，查找是否存在当前项
        while (iterator.hasNext())
        {
            Entry entry=iterator.next();
            if(entry.getKey()==key){
                entry.setValue(value);
                return;
            }
        }
        //如果不存在，则增加
        data[h].add(new Entry(key,value));
    }

    public int get(int key){
        int h = hash(key);
        Iterator<Entry> iterator = data[h].iterator();
        while(iterator.hasNext())
        {
            Entry entry = iterator.next();
            if(entry.getKey()==key)
            {
                return entry.getValue();
            }
        }
        return -1;
    }

    public void remove(int key){
        int h = hash(key);
        Iterator<Entry> iterator = data[h].iterator();
        while(iterator.hasNext()) {
            Entry entry = iterator.next();
            if (entry.getKey() == key) {
                data[h].remove(entry);
                return;
            }
        }
    }
}

```



> 参考资料：
>
> * [https://blog.csdn.net/m0_48795607/article/details/118946223](https://blog.csdn.net/m0_48795607/article/details/118946223)
> * [浅谈面试中HashMap相关问题](https://xiaoniuhululu.com/2022-04-18_interview-about-hashmap/)
> * [http://www.itabin.com/hashmap-first/](http://www.itabin.com/hashmap-first/)